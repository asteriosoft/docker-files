#!/bin/sh

/opt/druid/bin/start-nano-quickstart &

echo "Druid is starting, waiting 10 seconds"
sleep 10
echo "Kafka & Schema-registry are starting"

/opt/confluent/bin/kafka-server-start -daemon /opt/confluent/etc/kafka/server.properties
sleep 10
/opt/confluent/bin/schema-registry-start  -daemon /opt/confluent/etc/schema-registry/schema-registry.properties

tail -f /opt/druid/var/sv/coordinator-overlord.log